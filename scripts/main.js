jQuery(document).ready(function($) {
	
	// Voorkom dropdown functionaliteit van navigatiemenus op touchscreen apparaten
	$( '#nav-main li:has(ul)' ).doubleTapToGo();
	$( '#location li' ).doubleTapToGo();
	
	// Check of het scherm kleiner is dan 650 pixels (breekpunt mobiele layout)
	var mq = window.matchMedia( "(max-width: 650px)" );

	// initialiseer dorpdown functionaliteit voor 
	function mobilelayout() {
		if (mq.matches){
			$('#location-current').hover(function () {
				$('#location-select ul').css("display", "block")
			}, function () {
				$('#location-select ul').css("display", "none")
			});
			$('#location-select ul').hover(function () {
				$(this).css("display", "block")
			}, function () {
				$(this).css("display", "none")
			});
			$('#nav-button').hover(function () {
				$('#nav-main').css("display", "block")
			}, function () {
				$('#nav-main').css("display", "none")
			});
			$('#nav-main').hover(function () {
				$(this).css("display", "block")
			}, function () {
				$(this).css("display", "none")
			});
			var dropdownwidth = $('#location').width() - $('#nav-button').width()
			$('#location-select ul').width(dropdownwidth)
		}
		else {
			$('#location-select ul').removeAttr('style');
			$('#location-current').hover(function () {
				$('#location-select ul').css("display", "none")
			});
			$('#nav-main').removeAttr('style');
			$('#nav-main').hover(function () {
				$('#nav-main').css("display", "block")
			});	
		}
	}
	
	
	mobilelayout();
	
	$(window).resize(function() {
		mobilelayout();
	});
	
	// initieer de contentslider in de header van de pagina.

	$('#full-width-slider').royalSlider({
	  arrowsNav: true,
	  loop: true,
	  keyboardNavEnabled: false,
	  controlsInside: false,
	  imageScaleMode: 'fill',
	  slidesSpacing: 0,
	  autoScaleSliderWidth: 1280,
	  autoScaleSliderHeight: 440, 
	  arrowsNavAutoHide: false,
	  autoScaleSlider: true, 
	  controlNavigation: 'none',
	  thumbsFitInViewport: false,
	  navigateByClick: true,
	  transitionType:'move',
	  globalCaption: true,
	  sliderDrag: false,
	  transitionSpeed: 900,
	  autoPlay: {
    		enabled: false,
    		pauseOnHover: false,
			delay: 7500,
    	}
	})
	
	
	
	$('.royalSlider').on('click', '.slider-prev', function() {
		 $('.royalSlider').royalSlider('prev');	
	});
		
  	$('.royalSlider').on('click', '.slider-next', function() {
		 $('.royalSlider').royalSlider('next');
	
	});
	
	// Enable equal columns
	$('#events-info .grid').eqHeight('#events-info .equal');
	$('#content .grid').eqHeight('#content .equal');
		
});
